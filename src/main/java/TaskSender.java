import java.util.*;

/**
 * Класс создания задачи для запуска по времени
 */
public class TaskSender extends TimerTask {
    private Map<String, String> yesterdayLinkMap=new HashMap<>();
    private Map<String, String> todayLinkMap;
    private String email;

    TaskSender(Map<String, String> todayLinkMap,String email) {
        this.todayLinkMap = new HashMap<>(todayLinkMap);
        this.email=email;
    }

    @Override
    public void run() {
        Set<String> deleteLink = new HashSet();
        Set<String> newLink = new HashSet();
        Set<String> changedLink = new HashSet();
        for (String t : todayLinkMap.keySet()) {
            if (!yesterdayLinkMap.keySet().contains(t)) {
                newLink.add(t);
            }
        for (String y : yesterdayLinkMap.keySet()) {
            if (!todayLinkMap.keySet().contains(y)) {
                deleteLink.add(y);
            }
                if (y.equals(t) && !yesterdayLinkMap.get(y).equals(todayLinkMap.get(t))) {
                    changedLink.add(y);
                }
            }
        }
        EmailSender sender = new EmailSender(deleteLink, newLink, changedLink);
        sender.sendMessage(email);
        yesterdayLinkMap=new HashMap<>(todayLinkMap);
    }
}
