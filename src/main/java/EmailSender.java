import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;


/**
 * Класс отправки Email на почту (настроено на хост почты gmail)
 */
public class EmailSender {
    private static final String SMTP_HOST_NAME = "smtp.gmail.com";
    private static final String SMTP_PORT = "465";
    private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
    /**
     * Логин отправителя email (указать свой)
     */
    private static final String senderLogin = "sender@gmail.com";
    /**
     * Пароль отправителя email (указать свой)
     */
    private static final String senderPassword = "password";
    private Set<String> deleteLink;
    private Set<String> newLink;
    private Set<String> changedLink;

    EmailSender(Set<String> deleteLink, Set<String> newLink, Set<String> changedLink) {
        this.deleteLink = new HashSet(deleteLink);
        this.newLink = new HashSet(newLink);
        this.changedLink = new HashSet(changedLink);
    }

    /**
     * Отправка сообщения
     *
     * @param email адресат
     */
    public void sendMessage(String email) {
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.socketFactory.port", SMTP_PORT);
        properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        properties.setProperty("mail.smtp.host", SMTP_HOST_NAME);
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.port", SMTP_PORT);
        properties.put("mail.smtp.socketFactory.fallback", "false");

        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderLogin, senderPassword);
            }
        });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(senderLogin));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            message.setSubject("Ежедневный отчёт по изменениям сайтов");

            message.setText("Здравствуйте, дорогая и.о. секретаря\n\n" +
                    "За последние сутки во вверенных Вам сайтах произошли следующие изменения:\n" +
                    "Исчезли следующие страницы:" + deleteLink.toString() +
                    "\n" +
                    "Появились следующие новые страницы:" + newLink.toString() +
                    "\n" +
                    "Изменились следующие страницы:" + changedLink.toString() +
                    "\n\n" +
                    "С уважением,\n" +
                    "автоматизированная система мониторинга.");

            Transport.send(message);
            System.out.println("Email Sent successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }

    }

}