import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

/**
 * Главный класс запуска
 */
public class SenderTest {
    public static void main(String[] args) {
        Map<String, String> linkMap = new HashMap<String, String>();
        Timer timer = new Timer();
        linkMap.put("newLink", "WWWWW");
        linkMap.put("commonLink", "wwww");
        linkMap.put("changedLink", "ttt");
        linkMap.put("https://www.google.com",WebsiteWorker.getContent("https://www.google.com"));
        /**
         * Отправка email по времени
         */
        timer.schedule(new TaskSender(linkMap, "sended@gmail.com"), 5000, /*86400000*/20000);
    }
}
