import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Класс для работы с сайтами
 */
public class WebsiteWorker {

    /**
     * Считывание содержимого сайта
     * @param url - адрес сайта для считывания
     * @return содержимое сайта в виде строки
     */
    static String getContent(String url) {
        try {
            Document document = Jsoup.connect(url).get();
            return document.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
